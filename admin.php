<!DOCTYPE html>

<html lang="en">
<head>
      <meta charset="utf-8">

      <title> Dialect - Admin </title>
      <meta name="author" content="Rachel Singh">

      <link rel="stylesheet" href="assets/stylev2.css">
      <link rel="icon" type="image/png" href="assets/favicon.png">

      <script src="assets/jquery-3.5.0.min.js"></script>
      <script src="assets/script.js"></script>
  
    <link href="https://fonts.googleapis.com/css2?family=Courgette&family=Dosis&display=swap" rel="stylesheet"> 
</head>

<? include_once( "logic.php" ); ?>

<body class="admin">

    <div class="page-content">
        
        <div class="control-panel control-backdrop">
            <h1>Backdrop</h1>
            
            <div class="editing-group">
                <form method="post">
                    <div class="columns cf">
                        
                        <div class="column col-25">                            
                            Backdrop:<br>
                            <select name="backdrop-name">
                                <? foreach( $game->backdropInfo as $title => $info ) { ?>
                                <option value="<?=$title?>"><?=$title?></option>
                                <? } ?>
                            </select>
                        </div>
                        
                        <div class="column col-50">
                            Aspects:
                            <ol>
                                <li><input type="text" name="backdrop[aspect1]" value="<?=$game->sessionArray["backdrop"]["aspect1"]?>"></li>
                                <li><input type="text" name="backdrop[aspect2]" value="<?=$game->sessionArray["backdrop"]["aspect2"]?>"></li>
                                <li><input type="text" name="backdrop[aspect3]" value="<?=$game->sessionArray["backdrop"]["aspect3"]?>"></li>
                            </ol>
                        </div>
                        
                        <div class="column col-25">
                            Isolation name:<br>
                            <input type="text" name="backdrop[isolationName]" value="<?=$game->sessionArray["backdrop"]["isolationName"]?>"></p>
                        </div>
                        
                    </div> <!-- columns -->
                    
                    <p>
                        Notes:<br>
                        <textarea name="game-notes"><?=$game->sessionArray["notes"]?></textarea>
                    </p>
                    
                    <p><input type="submit" name="update-backdrop" value="Save"></p>
                </form>
            </div> <!-- editing-group -->
        </div>
        
        <div class="control-panel control-players">
            <h1>Players</h1>
            <h2>Add new player</h2>
            
            <h2>Update player</h2>
        </div>
        
        <div class="control-panel control-deck">
            <h1>Deck</h1>
            
            <h2>Deal cards</h2>
            
            <h2>Assign voice card</h2>
            
            <h2>Remove card</h2>
            
            <h2>Remove hands</h2>
        </div>
        
        <div class="control-panel control-words">
            <h1>Words</h1>
            
            <h2>Add word</h2>
            
            <h2>Update words</h2>
        </div>
        
        <div class="control-panel control-words">
            <h1>Resets</h1>
        </div>
        
        <div class="control-panel control-debug">
            <h1>Debug</h1>
<pre class="debug">
POST:
<? print_r( $_POST ); ?>

GET:
<? print_r( $_GET ); ?>

SESSION ID: <?= $game->session ?>

SESSION:
<? print_r( $game->sessionArray ); ?>

DEBUG LOG:
<? print_r( $game->status ); ?>
</pre>
        </div>
        
    </div>

</body>
