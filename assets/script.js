$( document ).ready( function() {
    // Refresh/idle timer
    var timeToRefresh = 10;
    var pageRefresh = setInterval( RefreshTimer, 1000 );
    var countIdle = 0;
    var pageIdle = null;
    
    if ( $( "#never-refresh" ).val() == "true" )
    {
        StopRefresh();
        StopIdle();
        $( "#refresh-info" ).addClass( "invisible" );
        $( "#idle-info" ).addClass( "invisible" );
        $( "#update-info" ).removeClass( "invisible" );
    }
    else
    {
        $( "#refresh-counter" ).html( timeToRefresh );
    }

    function RefreshPage() {
        $( "#invisible" ).submit();
    }

    function RefreshTimer() {
        timeToRefresh -= 1;
        console.log( timeToRefresh );

        $( "#refresh-counter" ).html( timeToRefresh );

        if ( timeToRefresh == 0 ) {
            RefreshPage();
        }
    }

    function StopRefresh() {
        clearInterval( pageRefresh );
        timeToRefresh = 0;
        $( "#refresh-counter" ).html( timeToRefresh );
        $( "#refresh-info" ).addClass( "invisible" );
        $( "#idle-info" ).removeClass( "invisible" );
        $( "#idle-counter" ).html( countIdle );

        if ( pageIdle == null ) {
            pageIdle = setInterval( IdleTimer, 1000 );
        }
    }

    function ResetIdleCounter() {
        countIdle = 0;
        if ( pageIdle == null ) {
            pageIdle = setInterval( IdleTimer, 1000 );
        }
    }

    function IdleTimer() {
        countIdle += 1;
        $( "#idle-counter" ).html( countIdle );

        if ( countIdle == 20 ) {
            // Restart refresh timer
            timeToRefresh = 10;
            pageRefresh = setInterval( RefreshTimer, 1000 );
            $( "#refresh-counter" ).html( timeToRefresh );
            clearInterval( pageIdle );
            $( "#refresh-info" ).removeClass( "invisible" );
            $( "#idle-info" ).addClass( "invisible" );
            pageIdle = null;
        }
    }

    function StopIdle() {
        clearInterval( pageIdle );
    }

    $( "input[type=text]" ).change( function() {
        StopRefresh();
        ResetIdleCounter();
    } );

    $( "#stop-refresh" ).click( function() {
        StopRefresh();
        StopIdle();
        $( "#refresh-info" ).addClass( "invisible" );
        $( "#idle-info" ).addClass( "invisible" );
        $( "#update-info" ).removeClass( "invisible" );
    } );

    $( this ).keypress( function() {
        StopRefresh();
        ResetIdleCounter();
    } );
    
    // Openable form
    $( ".open-menu-button" ).click( function() {
        console.log( "open menu" );
        var btnText = $( this ).val();
        if ( $( this ).next( ".open-menu" ).css( "display" ) == "none" ) {
            btnText = btnText.replace( "▼", "▲" );
            $( this ).next( ".open-menu" ).slideDown( "fast", function() { } );
            
        }
        else {
            btnText = btnText.replace( "▲", "▼" );
            $( this ).next( ".open-menu" ).slideUp( "fast", function() { } );
        }
        $( this ).val( btnText );
    } );
    
    /*
    $( "#add-new-player" ).click( function() {
        $( "#new-player-info" ).slideDown( "fast", function() { } );
        $( this ).fadeOut( "fast", function() { } );
    } );
    
    $( "#update-players" ).click( function() {
        $( "#update-player-info" ).slideDown( "fast", function() { } );
        $( this ).fadeOut( "fast", function() { } );
    } );
    */
    
    $( ".open-form" ).click( function() {
        if ( $( this ).next( ".open-me" ).css( "display" ) == "none" ) {
            $( this ).next( ".open-me" ).slideDown( "fast", function() { } );
        }
        else {
            $( this ).next( ".open-me" ).slideUp( "fast", function() { } );
        }
    } );
    
    $( ".open-me-toggle" ).click( function() {
        var text = $( this ).val();
        
        if ( $( this ).next( ".open-me" ).css( "display" ) == "none" ) {
            $( this ).next( ".open-me" ).slideDown( "fast", function() { } );
            text = text.replace( "►", "◄" );
            
        } else {
            $( this ).next( ".open-me" ).slideUp( "fast", function() { } );
            text = text.replace( "◄", "►" );
        }
        
        $( this ).val( text );
    } );
} );
