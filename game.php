<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Dialect </title>
  <meta name="description" content="Computer-readable conlang data for use to build APIs and other products">
  <meta name="author" content="Rachel Singh">

  <link rel="stylesheet" href="assets/style.css">
  <link rel="icon" type="image/png" href="assets/favicon.png">

  <script src="assets/jquery-3.5.0.min.js"></script>
  <script src="assets/script.js"></script>
</head>

<body>

<? include_once( "logic.php" ); ?>

<div class="game-view">
    
    <div class="game-notes-bar cf">
        <form method="post">
            <textarea name="game-notes"><?=$game->sessionArray["notes"]?></textarea>
            <br><br>
            <input type="submit" name="update-notes" value="Update notes">
        </form>
    </div>
    
    <div class="control-panel-bar cf">
        <div class="column backdrop-column">
            <input type="button" id="open-menu-backdrop" class="open-menu-button" value="Backdrop ▼">
            <div class="open-menu">
                <input type="button" id="setup-backdrop-btn" class="open-form" value="Backdrop settings">
                <form id="setup-backddrop" method="post" class="open-me cf">
                    
                    <h2>Backdrop<? if ( $game->sessionArray["backdrop"]["name"] != "" ) { echo( ": " . $game->sessionArray["backdrop"]["name"] ); } ?></h2>
                    <p><a href="http://www.moosader.com/web-games/dialect-tabletop/">View all backdrops</a></p>
                    
                    <p>
                        Backdrop info:
                    <? if ( $game->sessionArray["backdrop"]["link1"] != "" ) { ?>
                        <a href="<?=$game->sessionArray["backdrop"]["link1"]?>">Link 1</a>
                    <? } ?>
                    <? if ( $game->sessionArray["backdrop"]["link2"] != "" ) { ?>
                        <a href="<?=$game->sessionArray["backdrop"]["link2"]?>">Link 2</a>
                    <? } ?>
                    </p>
                    
                    <p>Our Isolation: <?=$game->sessionArray["backdrop"]["isolationName"]?></p>
                    
                    <p><sub>Backdrop name</sub><br>        
                    <input type="text" name="backdrop[name]" value="<?=$game->sessionArray["backdrop"]["name"]?>"></p>
                    
                    <p><sub>Backdrop links</sub> <br>
                    <input type="text" name="backdrop[link1]" value="<?=$game->sessionArray["backdrop"]["link1"]?>"><br>
                    <input type="text" name="backdrop[link2]" value="<?=$game->sessionArray["backdrop"]["link2"]?>">
                    </p>
                    
                    <br><p>Aspects</p>
                    <ol>
                        <li><input type="text" name="backdrop[aspect1]" value="<?=$game->sessionArray["backdrop"]["aspect1"]?>"></li>
                        <li><input type="text" name="backdrop[aspect2]" value="<?=$game->sessionArray["backdrop"]["aspect2"]?>"></li>
                        <li><input type="text" name="backdrop[aspect3]" value="<?=$game->sessionArray["backdrop"]["aspect3"]?>"></li>
                    </ol>
                    
                    <p><sub>The name of our Isolation</sub><br>        
                    <input type="text" name="backdrop[isolationName]" value="<?=$game->sessionArray["backdrop"]["isolationName"]?>"></p>
                    
                    <input type="submit" name="update-backdrop" value="Save">
                </form>
            </div> <!-- open menu -->
                
        </div> <!-- Backdrop -->
        
        <div class="column wordbuilding-column">
            <input type="button" id="open-menu-backdrop" class="open-menu-button" value="Word building ▼">
            <div class="open-menu">        
                <p>
                    Word building tips: <a href="rulebook/Dialect-page-046.jpg">Page 1</a> <a href="rulebook/Dialect-page-047.jpg">Page 2</a>
                </p>
                
                <input type="button" id="add-word-btn" class="open-form" value="Add word">
                <form id="add-word" method="post" class="open-me cf">
                    <p><sub>New word</sub><br>
                    <input type="text" name="vocabulary[word]" value="">
                    </p>
                    
                    <p><sub>Meaning</sub><br>
                    <input type="text" name="vocabulary[meaning]" value="">
                    </p>
                    
                    <br><p> Related Aspect(s) </p>
                    <ol>
                        <li><input type="checkbox" id="relate-aspect1" name="vocabulary[related][aspect1]" value="aspect1"> <label for="relate-aspect1"><?=$game->sessionArray["backdrop"]["aspect1"]?></label></li>
                        <li><input type="checkbox" id="relate-aspect2" name="vocabulary[related][aspect2]" value="aspect2"> <label for="relate-aspect2"><?=$game->sessionArray["backdrop"]["aspect2"]?></label></li>
                        <li><input type="checkbox" id="relate-aspect3" name="vocabulary[related][aspect3]" value="aspect3"> <label for="relate-aspect3"><?=$game->sessionArray["backdrop"]["aspect3"]?></label></li>
                    </ol>
                    
                    <br><p>Related Age</p>
                    <ol>
                        <li><input type="radio" id="relate-age1" name="vocabulary[age]" value="age1"> <label for="relate-age1">Age 1</label></li>
                        <li><input type="radio" id="relate-age2" name="vocabulary[age]" value="age2"> <label for="relate-age2">Age 2</label></li>
                        <li><input type="radio" id="relate-age3" name="vocabulary[age]" value="age3"> <label for="relate-age3">Age 3</label></li>
                        <li><input type="radio" id="relate-age4" name="vocabulary[age]" value="legacy"> <label for="relate-age4">Legacy</label></li>
                    </ol>
                    
                    <br><p class="instructions">
                        Think about why this Aspect has lead to this new word.
                        Have a conversation afterward with other players using this new word.
                    </p>
                    
                    <input type="submit" name="add-new-word" value="Add word">
                </form>
                
                <input type="button" id="change-word-btn" class="open-form" value="Update words">
                <form id="change-word" method="post" class="open-me cf">
                    <? foreach( $game->sessionArray["gameboard"] as $ageName => $ageData ) { ?>
                        <h3><?=$ageName?></h3>
                        <? foreach( $ageData as $key => $termData ) { ?> 
                            <div class="term-item">
                                <p><sub>Word</sub><br>
                                <input type="text" name="term[<?=$ageName?>][<?=$key?>][word]" value="<?=$termData["word"]?>"></p>
                                
                                <p><sub>Meaning</sub><br>
                                <input type="text" name="term[<?=$ageName?>][<?=$key?>][meaning]" value="<?=$termData["meaning"]?>"></p>
                            </div>
                        <? } ?>
                    <? } ?>
                    
                    <input type="submit" name="update-words" value="Update words">
                </form>
            </div>
        </div> <!-- Word building -->
        
        <div class="column deck-column">
            <input type="button" id="open-menu-backdrop" class="open-menu-button" value="Deck ▼">
            <div class="open-menu">
                <!-- DRAW CARDS -->
                <form id="draw-cards" method="post" class="open-me cf">
                    <p>
                        Draw <input type="text" class="number-input" name="how-many-cards" placeholder="1" value="1"> card(s)
                        of type...
                    </p>
                    <p>
                        <input type="radio" name="draw-type" id="draw-type-1" value="VOICE">   <label for="draw-type-1">Voice</label> <br>
                        <input type="radio" name="draw-type" id="draw-type-2" value="STORY-1"> <label for="draw-type-2">Story 1</label> <br>
                        <input type="radio" name="draw-type" id="draw-type-3" value="STORY-2"> <label for="draw-type-3">Story 2</label> <br>
                        <input type="radio" name="draw-type" id="draw-type-4" value="STORY-3"> <label for="draw-type-4">Story 3</label> <br>
                        <input type="radio" name="draw-type" id="draw-type-5" value="LEGACY">  <label for="draw-type-5">Legacy</label> <br>
                    </p>
                    
                    <input type="submit" name="draw-cards" value="Draw 'em">
                </form>
                
                <!-- DEAL CARDS -->
                <input type="button" id="deal-cards" class="open-form" value="Deal cards to players">
                <form id="deal-cards" method="post" class="open-me cf">
                    <p>
                        Deal <input type="text" class="number-input" name="deal[quantity]" placeholder="1" value="1"> card(s)
                        of type...
                    </p>
                    <p>
                        <input type="radio" name="deal[type]" id="deal-type-1" value="VOICE">   <label for="deal-type-1">Voice</label> <br>
                        <input type="radio" name="deal[type]" id="deal-type-2" value="STORY-1"> <label for="deal-type-2">Story 1</label> <br>
                        <input type="radio" name="deal[type]" id="deal-type-3" value="STORY-2"> <label for="deal-type-3">Story 2</label> <br>
                        <input type="radio" name="deal[type]" id="deal-type-4" value="STORY-3"> <label for="deal-type-4">Story 3</label> <br>
                        <input type="radio" name="deal[type]" id="deal-type-5" value="LEGACY">  <label for="deal-type-5">Legacy</label> <br>
                    </p>
                    <br><p>To player...</p>
                    <p>
                        <? foreach( $game->sessionArray["players"] as $key => $player ) { ?>
                        <input type="radio" name="deal[to]" id="deal-type-<?=$key?>" value="<?= $player["playerName"]?>">   <label for="deal-type-<?=$key?>"><?= $player["playerName"]?></label> <br>
                        <? } ?>
                        <input type="radio" name="deal[to]" id="deal-type-all" value="all"><label for="deal-type-all">All players</label>
                    </p>
                    
                    <input type="submit" name="deal-cards" value="Deal 'em">
                </form>
                
                
                <!-- REMOVE CARDS -->
                
                <input type="button" id="remove-card-btn" class="open-form" value="Remove card from player">
                <form id="remove-card" method="post" class="open-me cf">
                    <p>Remove which card...</p>

                        <? foreach( $game->sessionArray["players"] as $key => $player ) { ?>
                            
                        <br><p><strong><?=$player["playerName"]?>'s...</strong></p>
                        
                        <? foreach( $player["hand"] as $cardType => $cardsWithType ) { ?>
                            <? foreach( $cardsWithType as $cardkey => $cardIndex ) { ?> 
                                <? 
                                $card = $game->GetCardWithTypeAndIndex( $cardType, $cardIndex );
                                $uniqueId = $player["playerName"] . "_" . $cardType . "_" . $cardIndex;
                                $playerId = $game->GetIndexOfPlayerWithName( $player["playerName"] );
                                ?>
                                <input type="radio"  name="removeCard[<?=$uniqueId?>][checked]" id="<?=$uniqueId?>" value="checked">
                                
                                <input type="hidden" name="removeCard[<?=$uniqueId?>][cardTitle]"   value="<?=$card["TITLE"]?>">
                                <input type="hidden" name="removeCard[<?=$uniqueId?>][cardKey]"     value="<?=$cardIndex?>">
                                <input type="hidden" name="removeCard[<?=$uniqueId?>][cardType]"    value="<?=$cardType?>">
                                <input type="hidden" name="removeCard[<?=$uniqueId?>][playerName]"  value="<?=$player["playerName"]?>">
                                <input type="hidden" name="removeCard[<?=$uniqueId?>][playerId]"    value="<?=$playerId?>">
                                
                                <label for="<?=$uniqueId?>"><?=$card["TITLE"] ?></label> <br>
                            <? } ?>
                        <? } ?>
                        

                        <? } ?>
                    <input type="submit" name="remove-card-from-player" value="Remove it!">
                </form>
                
                <input type="button" id="remove-cards" class="open-form" value="Remove hands from players">
                <form id="remove-cards" method="post" class="open-me cf">
                    <p>Remove card types...</p>
                    <p>
                        <input type="radio" name="remove[type]" id="remove-type-1" value="VOICE">   <label for="remove-type-1">Voice</label> <br>
                        <input type="radio" name="remove[type]" id="remove-type-2" value="STORY-1"> <label for="remove-type-2">Story 1</label> <br>
                        <input type="radio" name="remove[type]" id="remove-type-3" value="STORY-2"> <label for="remove-type-3">Story 2</label> <br>
                        <input type="radio" name="remove[type]" id="remove-type-4" value="STORY-3"> <label for="remove-type-4">Story 3</label> <br>
                        <input type="radio" name="remove[type]" id="remove-type-5" value="LEGACY">  <label for="remove-type-5">Legacy</label> <br>
                    </p>
                    <br><p>From player...</p>
                    <p>
                        <? foreach( $game->sessionArray["players"] as $key => $player ) { ?>
                        <input type="radio" name="remove[from]" id="remove-type-<?=$key?>" value="<?= $player["playerName"]?>">   <label for="remove-type-<?=$key?>"><?= $player["playerName"]?></label> <br>
                        <? } ?>
                        <input type="radio" name="remove[from]" id="remove-type-all" value="all"><label for="remove-type-all">All players</label>
                    </p>
                    
                    <input type="submit" name="remove-cards" value="Remove 'em">
                </form>
                

                <input type="button" id="assign-voice-card" class="open-form" value="Assign Voice cards">
                <form id="assign-voice-card" method="post" class="open-me cf">
                    <p>
                        Draw 3 voice cards for each player. The player gets to choose which of the 3 will identify their character.
                        Enter in their selections here.
                    </p>
                    
                    <? foreach( $game->sessionArray["players"] as $key => $player ) { ?>
                        <? $card = $game->cardsArray["VOICE"][$player["voiceCardIndex"]]; ?>
                        <div class="player-info">
                            <p>
                                <sub>Player name:</sub><br>
                                <input type="text" name="assignVoice[<?=$key?>][playerName]" value="<?=$player["playerName"]?>">
                            </p>
                            <p>
                                <sub>Voice card:</sub><br>
                                <input type="text" name="assignVoice[<?=$key?>][voiceCardName]" value="<?=$card["TITLE"]?>">
                            </p>
                        </div>
                        <hr>
                    <? } ?>

                    <input type="submit" name="assign-voice-cards" value="Assign Voice cards">
                </form>
            </div>
            
        </div> <!-- Deck -->
        
        <div class="column player-column">
            <input type="button" id="open-menu-backdrop" class="open-menu-button" value="Players ▼">
            <div class="open-menu">
                <input type="button" id="add-new-player" class="open-form" value="Add new player">
                <form id="new-player-info" method="post" class="open-me cf">
                    <p>
                        <sub>New player name:</sub><br>
                        <input type="text" name="new-player-name">
                    </p>

                    <input type="submit" name="add-new-player" value="Add">
                </form>

                <input type="button" id="update-players" class="open-form" value="Update players">
                <form id="update-player-info" method="post" class="open-me cf">
                    <? foreach( $game->sessionArray["players"] as $key => $player ) { ?>
                        <div class="player-info">
                            <p>
                                <sub>Player name:</sub><br>
                                <input type="text" name="update[<?=$key?>][playerName]" value="<?=$player["playerName"]?>">
                            </p>
                            <p>
                                <sub>Player pronouns:</sub><br>
                                <input type="text" name="update[<?=$key?>][playerPronouns]" value="<?=$player["playerPronouns"]?>">
                            </p>
                            <p>
                                <sub>Character name:</sub><br>
                                <input type="text" name="update[<?=$key?>][characterName]" value="<?=$player["characterName"]?>">
                            </p>
                            <p>
                                <sub>Character pronouns:</sub><br>
                                <input type="text" name="update[<?=$key?>][characterPronouns]" value="<?=$player["characterPronouns"]?>">
                            </p>
                            <p>
                                <sub>Character nickname:</sub><br>
                                <input type="text" name="update[<?=$key?>][characterNickname]" value="<?=$player["characterNickname"]?>">
                            </p>
                        </div>
                        <hr>
                    <? } ?>

                    <input type="submit" name="update-players" value="Update">
                </form>
            </div>

        </div> <!-- Player management -->
        
        <div class="column reset-column">
            <input type="button" id="open-menu-resets" class="open-menu-button" value="Resets ▼">
            <div class="open-menu">
                <form id="reset-game" method="post" class="cf">
                    <input type="submit" name="reset-cards" value="Reset cards" class="reset reset-cards">
                    <input type="submit" name="reset-game" value="Reset game" class="reset reset-game">
                </form>
                
                <hr>
                
                <p>Debug</p>
                <pre class="debug">
                POST:
                <? print_r( $_POST ); ?>

                GET:
                <? print_r( $_GET ); ?>

                SESSION ID: <?= $game->session ?>

                SESSION:
                <? print_r( $game->sessionArray ); ?>

                DEBUG LOG:
                <? print_r( $game->status ); ?>

                <? //print_r( $game->cardsArray ); ?>
                </pre>

                <pre class="debug2">
                CARDS:
                <? print_r( $game->cardsArray ); ?>
                </pre>
            </div>
            </div>
        </div> <!-- reset column -->
        
        
    </div> <!-- Control panel bar -->
    
        
        
    
<div class="column-board">
    <div class="background-info">

        Backdrop: <?=$game->sessionArray["backdrop"]["name"]?> |
	<?=$game->sessionArray["backdrop"]["isolationName"]?> 
        | 
        
        <? if ( $game->sessionArray["backdrop"]["link1"] != "" ) { ?>
            <a href="<?=$game->sessionArray["backdrop"]["link1"]?>">Link 1</a>
        <? } ?> |
        <? if ( $game->sessionArray["backdrop"]["link2"] != "" ) { ?>
            <a href="<?=$game->sessionArray["backdrop"]["link2"]?>">Link 2</a>
        <? } ?>
    </div>
    <div class="aspect-list cf">
        <div class="aspect aspect1"><h3>Aspect 1</h3><?=$game->sessionArray["backdrop"]["aspect1"]?></div>
        <div class="aspect aspect2"><h3>Aspect 2</h3><?=$game->sessionArray["backdrop"]["aspect2"]?></div>
        <div class="aspect aspect3"><h3>Aspect 3</h3><?=$game->sessionArray["backdrop"]["aspect3"]?></div>
    </div>
    <? foreach( $game->sessionArray["gameboard"] as $ageName => $ageData ) { ?>
    <div class="age-board age-1 cf">
        <p class="age-name"><?=$ageName?></p>
        
        <div class="age-cards cf">
            <? foreach( $ageData as $key => $card ) { ?> 
                <div class="game-piece user-card">
                    
                    <form method="post" id="close-window">
                        <input type="submit" class="close-window" name="remove-term-card" value="X">
                        <input type="hidden" name="removeTerm[age]" value="<?=$ageName?>">
                        <input type="hidden" name="removeTerm[cardIndex]" value="<?=$key?>">
                    </form>
                    
                    <p>
                        <sub>Term</sub><br>
                        <span class="word"><?=$card["word"]?></span>
                    </p>
                    <p>
                        <sub>Meaning</sub><br>
                        <span class="meaning"><?=$card["meaning"]?></span>
                    </p>
                    <p class="aspects">
                        Related aspects:
                        <ul>
                            <? foreach( $card["relatedAspects"] as $aspect ) { ?>
                                <li><?=$game->sessionArray["backdrop"][$aspect]?></li>
                            <? } ?>
                        </ul>
                    </p>
                </div>
            <? } ?>
        </div>
        
    </div>
    <? } ?>
</div>

<div class="column-refresh">

    <div id="refresh-info" class="info ">Refresh: <span id="refresh-counter">x</span>
    <sub></sub>
    </div>
    <div id="idle-info" class="info invisible">Idle: <span id="idle-counter">x</span>
    <sub></sub>
    </div>
    <div id="update-info" class="info invisible">
        You will need to manually refresh the page to see updates made by other players.
    </div>
    <br>    
    <? if ( !isset( $_GET["norefresh"] ) ) { ?>
    <form id="invisible" name="nothing" method="post">
        <a href="http://www.moosader.com/web-games/dialect-tabletop/game.php?norefresh=true" class="button">Never auto-refresh</a>
        <input type="button" id="stop-refresh" value="Stop auto-refresh" class="button">
        <input type="submit" value="Manual refresh" class="button">
    </form>
    <? } ?>
    
    <? if ( isset( $_GET["norefresh"] ) ) { ?>
        <input type="hidden" id="never-refresh" value="true">
    <? } else { ?>
        <input type="hidden" id="never-refresh" value="false">
    <? } ?>
</div>


<? if ( sizeof( $game->sessionArray["drawnCards"] ) > 0 ) { ?>
<div class="column-drawn-cards card-row cf">
    <form method="post" id="close-drawn-cards">
        <input type="submit" class="close-window" name="close-drawn-cards" value="X">
    </form>
    <h2>Drawn cards</h2>
    <? foreach( $game->sessionArray["drawnCards"] as $key => $cardInfo ) { ?>
        <? $card = $game->cardsArray[$cardInfo["type"]][$cardInfo["index"]]; ?>
        <? if ( $cardInfo["type"] == "VOICE" ) { ?>
            <div class="game-piece card voice">
                <p><span class="title">
                    <?= $card["TITLE"] ?>
                </span></p>
                <p><span class="description">
                    <?= $card["DESCRIPTION"] ?>
                </span></p>
                <div class="bottom-spot">
                <p><span class="description2">
                <?= $card["DESCRIPTION2"] ?>
                </span></p>
                </div>
            </div>
        <? } else if ( $cardInfo["type"] == "LEGACY" ) { ?>
            <div class="game-piece card story">
                <p><span class="title">
                    <?= $card["TITLE"] ?>
                </span></p>
                <p><span class="description">
                    <?= $card["DESCRIPTION"] ?>
                </span></p>
                <div class="bottom-spot">
                <p><span class="description2">
                <?= $card["DESCRIPTION2"] ?>
                </span></p>
                </div>
            </div>
        <? } else { ?>
            <div class="game-piece card legacy">
                <p><span class="description">
                    <?= $card["DESCRIPTION"] ?>
                </span></p>
            </div>
        <? } ?>
    <? } ?>
</div>
<? } ?>

<?
/*********************************************************************** Game is open */
?>
<div class="column-players">
    <? foreach( $game->sessionArray["players"] as $key => $player ) { ?>
    <div class="player">
        <? if ( isset( $player["voiceCardIndex"] ) ) { ?>
            <? $voiceCard = $game->cardsArray["VOICE"][$player["voiceCardIndex"]]; ?>
        <? } ?>
        
        <div class="spacing">
            
            <table class="player-name">
                <tr class="player-info">
                    <td>Player</td>
                        <td><?=$player["playerName"]?> 
                        <sub><?=$player["playerPronouns"]?></sub>
                    </td>
                </tr>
                <tr class="character-info">
                    <td>Character</td>
                    <td>
                        <strong><?=$player["characterName"]?></strong>
                        <em>&quot;<?=$player["characterNickname"]?>&quot;</em>
                        <sub><?=$player["characterPronouns"]?></sub>
                    </td>
                </tr>
                <tr class="character-info">
                    <td>Voice</td>
                    <td><?=$voiceCard["TITLE"]?></td>
                </tr>
            </table>
                
            <br><input type="button" class="open-me-toggle" value="View hand ►">
            <div class="open-me">
                <div class="all-cards cf">
                    <!-- Display voice card first -->
                    <? if ( isset( $voiceCard ) ) { ?>
                        <hr><p>Assigned Voice:</p>
                        
                        <input type="button" class="open-menu-button card-toggle" value="<?= $voiceCard["TITLE"] ?> ▼">
                        <div class="open-menu cf">
                            <div class="game-piece card voice">
                                <p><span class="title">
                                    <?= $voiceCard["TITLE"] ?>
                                </span></p>
                                <p><span class="description">
                                    <?= $voiceCard["DESCRIPTION"] ?>
                                </span></p>
                                <div class="bottom-spot">
                                <p><span class="description2">
                                <?= $voiceCard["DESCRIPTION2"] ?>
                                </span></p>
                                </div>
                            </div>
                        </div> <!-- menu toggle -->
                        
                    <? } ?>
                    <!-- Display other cards in their hand -->
                    <? foreach( $player["hand"] as $type => $cardArray ) { 
                        if ( sizeof( $cardArray ) == 0 ) { continue; }
                        ?>
                        <div class="card-row cf">
                        <hr>                       
                        
                        <input type="button" class="open-menu-button card-toggle" value="<?=$type?> Cards ▼">
                        <div class="open-menu cf">
                            <? foreach ( $cardArray as $index => $cardKey ) { 
                                $card = $game->cardsArray[$type][$cardKey];
                                ?>
                                <div class="game-piece card voice">
                                    <p><span class="title">
                                        <?= $card["TITLE"] ?>
                                    </span></p>
                                    <p><span class="description">
                                        <?= $card["DESCRIPTION"] ?>
                                    </span></p>
                                    <div class="bottom-spot">
                                    <p><span class="description2">
                                    <?= $card["DESCRIPTION2"] ?>
                                    </span></p>
                                    </div>
                                </div>
                            <? } ?>
                        </div>
                            
                        </div>
                    <? } ?>
                </div>
            </div>

        </div>
        
        </div> <!-- player -->
    <? } ?>
</div>

<? if ( isset( $game->sessionArray["safety"] ) ) { ?>
    <div class="column-safety-notice">
        <form method="post" id="close-window">
            <input type="submit" class="close-window" name="close-safety-flag" value="X">
        </form>
        The X button has been pressed!
    </div>
<? } ?>


    <form class="safety safety-window" method="post" name="safety-button">
        <div class="spacing">
            <p><strong>Safety</strong></p>
            <p>
                If anything makes anyone uncomfortable in any way, tap the X button
                and we will edit out anything. You don't have to explain why.
                This is here to ensure everyone feels safe and comfortable playing.
            </p>
            <input type="submit" class="safety-card" name="safety-card" value="X">
        </div>
    </form>


<!--
    <hr>

    <div class="column-decks cf">
        <form method="post" name="draw-card">
            <input type="submit" name="draw-voice"  title="Draw a Voice card"   class="draw-card voice-card"  value="voice">
            <input type="submit" name="draw-story1" title="Draw a Story 1 card" class="draw-card story1-card" value="story1">
            <input type="submit" name="draw-story2" title="Draw a Story 2 card" class="draw-card story2-card" value="story2">
            <input type="submit" name="draw-story3" title="Draw a Story 3 card" class="draw-card story3-card" value="story3">
            <input type="submit" name="draw-legacy" title="Draw a Legacy card"  class="draw-card legacy-card" value="legacy">
        </form>
    </div>
-->
    <!--
    
    <div class="cheatsheet">
            
        <h2>Cheatsheet</h2>
        <p><a href="rulebook/Dialect-page-076.jpg">Page 1</a> | <a href="rulebook/Dialect-page-077.jpg">Page 2</a></p>


        <ol>
            <li>
                Create the Isolation
                <ol>
                    <li>Pick a Backdrop</li>
                    <li>Generate Aspects</li>
                    <li>Answer Community Questions</li>
                    <li>Name the Isolation</li>
                </ol>
            </li>
            
            <li>
                Create Characters
                <ol>
                    <li>Pick an Archetype card (Voice cards)</li>
                    <li>Create a character name and nickname</li>
                    <li>Given an introduction</li>
                </ol>
            </li>
            
            <li>
                Gameplay
                <ol>
                    <li>Beginning: Deal 3 Age-1 cards</li>
                    <li>
                        Each player's turn...
                        <ol>
                            <li>Make a connection - play a card on one of the Aspects from the current Age. Explain why the Isolation's language for the concept on the card has emerged from that Aspect.</li>
                            <li>Build a Word - Communally build a word for the concept together.</li>
                            <li>Have a Conversation - The player and others they choose have a conversation prompted by the text at the bottom of the card. Demonstrate usage of the new term.</li>
                            <li>Draw a card for the next Age (except when in Age 3)</li>
                        </ol>
                    </li>
                    <li>
                        After every player has gone, the Age ends...
                        <ol>
                            <li>Read the Transition prompt</li>
                            <li>Read the next step of the Pathway.</li>
                            <li>Evolve an Aspect - Decide an Aspect that changes due to the Transition.</li>
                            <li>Optional - Discard a card in your hand for a card from the new age.</li>
                        </ol>
                    </li>
                    <li>
                        Once we've finished Age 3...
                        <ol>
                            <li>Each player receives 1 Legacy card</li>
                            <li>Each player narrates their epilogue</li>
                        </ol>
                    </li>
                </ol>
            </li>
        </ol>
    </div>
    
    -->
    <hr>
    




</div> <!-- game-view -->

</body>
