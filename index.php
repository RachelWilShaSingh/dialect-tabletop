<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title> Dialect </title>
    <meta name="description" content="Homepage for Rachel's virtual Dialect tabletop">
    <meta name="author" content="Rachel Singh">

    <link rel="stylesheet" href="assets/stylev2.css">
    <link rel="icon" type="image/png" href="assets/favicon.png">

    <link href="https://fonts.googleapis.com/css2?family=Courgette&family=Dosis&display=swap" rel="stylesheet"> 
</head>

<body class="homepage">
    <div class="page-content">
        <p>
            <span class="game-title">Dialect: A game about language and how it dies</span><br>
            <a href="https://thornygames.com/pages/dialect">(Official Dialect page @ Thorny Games)</a>
        </p>
        
        <p>
            Dialect is a roleplaying game that takes about three hours to play through.
            In it, you and the other player characters are part of some sort of <em>Isolation</em> -
            this can be anything from being abandoned robots on a mining colony in space, 
            to a group of schoolboys at a boarding school. A shared experience builds
            a shared bond and a shared language.
        </p>
        
        <p>
            In the game, a Backdrop is selected to begin building the story on.
            The game lasts for Three Ages, where at the end of each Age some event occurs.
        </p>
        
        <hr>
        
        
        <h2>Backdrops</h2>
        <p>
            Once of these story backdrops will be chosen for a game session.
        </p>
        
        <table class="scenarios">
            <tr>
                <td colspan="3">
                    <h3>Core Backdrops (part of the guidebook):</h3>
                </td>
            </tr>
            
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Pages</th>
            </tr>
            <tr>
                <td>The Outpost:</td> 
                <td>The story of the first Martian outpost. After
communications with Earth have been cut off, they fight to survive on the
Red Planet.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-080.jpg">1</a>, <a href="rulebook/Dialect-page-081.jpg">2</a> </td> 
            </tr>
            <tr>
                <td>The Compound: </td> 
                <td>A group has barricaded itself against a world
they can no longer take part in. The year is 1982 and in their solitude, they
will create a new Utopian home.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-082.jpg">1</a>, <a href="rulebook/Dialect-page-083.jpg">2</a> </td> 
            </tr>
            <tr>
                <td>Sing the Earth Electric: </td> 
                <td>Left behind for one final mission
before humanity abandoned Earth, a misfit crew of machines pursue their
ultimate duty.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-084.jpg">1</a>, <a href="rulebook/Dialect-page-085.jpg">2</a> </td> 
            </tr>
            <tr>
                <td>Thieves' Cant: </td> 
                <td>Scoundrels and thieves must develop a way
to communicate with each other in public without betraying their real
intentions.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-086.jpg">1</a>, <a href="rulebook/Dialect-page-087.jpg">2</a> </td> 
            </tr>
            
            <tr>
                <td colspan="3">
                    <h3>Contributed Backdrops (part of the guidebook):</h3>
                </td>
            </tr>
            
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Pages</th>
            </tr>    
            <tr>
                <td>Sancutary Island: </td>
                <td>The sickness tore through
Boston. In the shadow of the lighthouse on their island home, this group is
determined to remain untouched by the disease.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-092.jpg">1</a>, <a href="rulebook/Dialect-page-093.jpg">2</a> </td>
            </tr>
            <tr>
                <td>The Czaten Dacha: </td>
                <td>Always on the move,
they make their homes in their carts and caravans. Distrusted by outsiders,
locals refer to them as the Czaten Dacha, or horse people.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-094.jpg">1</a>, <a href="rulebook/Dialect-page-095.jpg">2</a> </td>
            </tr>
            <tr>
                <td>Worcester School: </td>
                <td>They were sent to a
remote English boarding school to become men. Instead, in their isolation,
they found identity, community, and resistance.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-096.jpg">1</a>, <a href="rulebook/Dialect-page-097.jpg">2</a> </td>
            </tr>
            <tr>
                <td>Wolf Pack: </td>
                <td>They protect their pack and
their forest. Hunters in the night, the wolves use their bodies as well as their
voices to communicate.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-098.jpg">1</a>, <a href="rulebook/Dialect-page-099.jpg">2</a> </td>
            </tr>
            <tr>
                <td>2081, Solar Slums: </td>
                <td>In these neon streets, refusing
the cultural suicide of corporate life means making your own way. Shaded
from the deadly sun, this resistance of cyborgs and hackers will fight with
every tool they can against a consumerist dictatorship.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-100.jpg">1</a>, <a href="rulebook/Dialect-page-101.jpg">2</a> </td>
            </tr>
            <tr>
                <td>The Protecting Ones: </td>
                <td>The Swimming
Ones, the Flying Ones, and the Crawling Ones are in danger. It is up to the
Protecting Ones to safeguard the waters of the world.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-102.jpg">1</a>, <a href="rulebook/Dialect-page-103.jpg">2</a> </td>
            </tr>
            <tr>
                <td>Slave Uprising: </td>
                <td>They claimed their freedom
in blood. With their captors now buried on the island shores, how will they
rule over their new home?</td>
                <td>View scenario: <a href="rulebook/Dialect-page-104.jpg">1</a>, <a href="rulebook/Dialect-page-105.jpg">2</a> </td>
            </tr>
            <tr>
                <td>Forbidden Children: </td>
                <td>Unseen and discarded, these children
must move in the sprawling hidden spaces of the city. May they have a
moment to dream in the cracks and shadows.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-106.jpg">1</a>, <a href="rulebook/Dialect-page-107.jpg">2</a> </td>
            </tr>
            <tr>
                <td>Velayuthapuram, Tamil Nadu 2006: </td>
                <td>Punished
for no crime, this Dalit community in rural India is imprisoned in their own
home. They must relish small victories and moments of happiness in order
to survive in the face of cruel oppression.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-108.jpg">1</a>, <a href="rulebook/Dialect-page-109.jpg">2</a> </td>
            </tr>
            <tr>
                <td>Beyond the Village: </td>
                <td>This band of queer artists,
laborers, and hustlers make their homes in former parking garages of an
urban hellscape that stretches forever. They must nurture and care for one
another while fighting back against those who would prey on them.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-110.jpg">1</a>, <a href="rulebook/Dialect-page-111.jpg">2</a> </td>
            </tr>
            <tr>
                <td>The Self-Actualizataion Project: </td>
                <td>They didn’t
fit in with the rest of the world. Scattered across the globe, but connected
digitally, they will form new lives with their families of choice.</td>
                <td>View scenario: <a href="rulebook/Dialect-page-112.jpg">1</a>, <a href="rulebook/Dialect-page-113.jpg">2</a> </td>
            </tr>
            <tr>
                <td>Toybox Tales: </td>
                <td>As the years go on,
playtime grows shorter. What is to become of the toys when the Child they
love so dearly grows up?</td>
                <td>View scenario: <a href="rulebook/Dialect-page-114.jpg">1</a>, <a href="rulebook/Dialect-page-115.jpg">2</a> </td>
            </tr>
            
            
            
            
            
            
            
            <tr>
                <td colspan="3">
                    <h3>Rachel's Backdrops (Silly/light settings)</h3>
                </td>
            </tr>
            
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Pages</th>
            </tr>
            
            <tr>
                <td>The Quest:</td>
                <td>A Final Fantasy inspired backdrop.</td>
                <td>View scenario:  <a href="scenarios/custom-the-quest.jpg">1</a></td>
            </tr>
            <tr>
                <td>Survival Island:</td>
                <td>A Reality TV show inspired backdrop.</td>
                <td>View scenario:  <a href="scenarios/custom-survival-island.jpg">1</a></td>
            </tr>
        </table>
        
        
    </div>

</body>
</html>
