<?
function Err( $message ) {
    echo( "<p class='error'>$message</p>" );
}

function PhpDebug( $label, $message ) {
    echo( "<!--\n" . $label . ": " . $message . "\n-->\n" );
}

function PhpDebugArray( $label, $arr ) {
    echo( "<!--\n" );
    echo( $label . ":\n" );
    print_r( $arr );
    echo( "\n-->\n\n" );
}

class Game
{
    public $cardsPath;
    public $cardsArray;
    public $sessionPath;
    public $session;
    public $sessionArray;
    public $status;
    public $backdropInfo;

    function __construct() {
        $this->cardsPath = "data/cards.csv";
        $this->LoadCards();

        $this->status = array();

        $this->sessionPath = "sessions";
        $this->sessionArray = null;

        $this->session = "test";
        
        $this->SetupBackdropInfo();

        // Load latest with changes
        $this->LoadGameState();

        if ( isset( $_POST["add-new-player"] ) ) {
            $this->AddNewPlayer( $_POST["new-player-name"] );
        }
        if ( isset( $_POST["update-players"] ) ) {
            $this->UpdatePlayers( $_POST["update"] );
        }
        if ( isset( $_POST["assign-voice-cards"] ) ) {
            $this->AssignVoiceCards( $_POST["assignVoice"] );
        }
        if ( isset( $_POST["deal-cards"] ) ) {
            $this->DealCards( $_POST["deal"] );
        }
        if ( isset( $_POST["draw-cards"] ) ) {
            $this->DrawCards( $_POST["draw-type"], $_POST["how-many-cards"] );
        }
        if ( isset( $_POST["draw-voice"] ) ) {
            $this->DrawCards( "VOICE", 1 );
        }
        if ( isset( $_POST["draw-story1"] ) ) {
            $this->DrawCards( "STORY-1", 1 );
        }
        if ( isset( $_POST["draw-story2"] ) ) {
            $this->DrawCards( "STORY-2", 1 );
        }
        if ( isset( $_POST["draw-story3"] ) ) {
            $this->DrawCards( "STORY-3", 1 );
        }
        if ( isset( $_POST["draw-legacy"] ) ) {
            $this->DrawCards( "LEGACY", 1 );
        }
        if ( isset( $_POST["close-drawn-cards"] ) ) {
            $this->ClearDrawnCards();
        }
        if ( isset( $_POST["update-backdrop"] ) ) {
            $this->UpdateBackdrop( $_POST["backdrop"] );
        }
        if ( isset( $_POST["reset-cards"] ) ) {
            $this->ClearCardsUsed();
        }
        if ( isset( $_POST["reset-game"] ) ) {
            $this->StartNewGame();
        }
        if ( isset( $_POST["remove-cards"] ) ) {
            $this->RemoveCardsFromPlayers( $_POST["remove"] );
        }
        if ( isset( $_POST["safety-card"] ) ) {
            $this->FlagSafety();
        }
        if ( isset( $_POST["close-safety-flag"] ) ) {
            $this->RemoveSafety();
        }
        if ( isset( $_POST["add-new-word"] ) ) {
            $this->AddNewWord( $_POST["vocabulary"] );
        }
        if ( isset( $_POST["remove-card-from-player"] ) ) {
            $this->RemoveCardFromPlayer( $_POST["removeCard"] );
        }
        if ( isset( $_POST["remove-term-card"] ) ) {
            $this->RemoveTermCard( $_POST["removeTerm"] );
        }
        if ( isset( $_POST["update-words"] ) ) {
            $this->UpdateWords( $_POST["term"] );
        }
        if ( isset( $_POST["update-notes"] ) ) {
            $this->UpdateNotes( $_POST["game-notes"] );
        }

        // Load latest with changes
        $this->LoadGameState();
    }

    function Log( $data ) {
        array_push( $this->status, $data );
    }

    function IsGameLoaded() {
        return ( isset( $this->session ) );
    }

    function LoadCards() {
        $this->Log( "Load cards" );
        $this->cardsArray = array(
            "VOICE" => array(),
            "STORY-1" => array(),
            "STORY-2" => array(),
            "STORY-3" => array(),
            "LEGACY" => array()
        );

        $columnHeaders = array();
        if ( ( $fileHandler = fopen( $this->cardsPath, "r" ) ) !== false ) {
            $columnHeaders = fgetcsv( $fileHandler );

            while ( ( $row = fgetcsv( $fileHandler ) ) !== false ) {
                $cardData = array();
                foreach( $row as $key => $value ) {
                    $cardData[$columnHeaders[$key]] = $value;
                }

                array_push( $this->cardsArray[ $cardData["CARD TYPE"] ], $cardData );
            }

            fclose( $fileHandler );
        }
        else {
            Err( "Couldn't open cards path \"" . $this->cardsArray . "\"!" );
        }
    }

    function StartNewGame() {
        // Create a session ID
        $this->session = $this->MakeSessionID();

        // Set up the session array
        $this->sessionArray = array();
        $this->sessionArray["session"] = $this->session;
        $this->sessionArray["players"] = array();
        $this->sessionArray["cardsUsed"] = array(
            "VOICE" => array(),
            "STORY-1" => array(),
            "STORY-2" => array(),
            "STORY-3" => array(),
            "LEGACY" => array()
        );

        // Save it
        $this->SaveGameFile();
    }

    function MakeSessionID() {
        return "test"; // date( "YmdHis" )
    }

    function LoadGameState() {
        // Load the game file
        $this->LoadGameFile();
    }

    function SaveGameFile() {
        $this->Log( "Save the game file" );
        $jsonString = json_encode( $this->sessionArray );
        file_put_contents( $this->GetSessionPath(), $jsonString );
    }

    function SaveGameState() {
        $this->SaveGameFile();
    }

    function LoadGameFile() {
        $this->Log( "Load the game file" );
        if ( !file_exists( $this->GetSessionPath() ) ) {
            $this->StartNewGame();
        }
        $jsonString = file_get_contents( $this->GetSessionPath() );
        $this->sessionArray = json_decode( $jsonString, true );
    }

    function GetSessionPath() {
        return $this->sessionPath . "/" . $this->session . ".json";
    }

    function AddNewPlayer( $name ) {
        $this->Log( "Add new player \"" . $name . "\"" );
        $playerInfo = array(
            "playerName" => $name,
            "characterName" => "",
            "characterNickname" => "",
            "voiceCardIndex" => "",
            "hand" => array(
                "VOICE" => array(),
                "STORY-1" => array(),
                "STORY-2" => array(),
                "STORY-3" => array(),
                "LEGACY" => array()
            )
        );
        array_push( $this->sessionArray["players"], $playerInfo );

        $this->SaveGameState();
    }

    function UpdatePlayers( $infoArray ) {
        $this->Log( "Update players" );
        foreach ( $infoArray as $key => $player ) {
            $this->Log( "Player ID " . $key . "..." );

            $this->sessionArray["players"][$key]["playerName"] = $player["playerName"];
            $this->sessionArray["players"][$key]["characterName"] = $player["characterName"];
            $this->sessionArray["players"][$key]["characterNickname"] = $player["characterNickname"];
            $this->sessionArray["players"][$key]["playerPronouns"] = $player["playerPronouns"];
            $this->sessionArray["players"][$key]["characterPronouns"] = $player["characterPronouns"];
        }

        $this->SaveGameState();
    }

    function AssignVoiceCards( $infoArray ) {
        foreach( $infoArray as $playerKey => $updateInfo ) {
            $this->Log( "Player \"" . $updateInfo["playerName"] . "\" wants card \"" . $updateInfo["voiceCardName"] . "\"" );

            $voiceCardIndex = $this->GetIndexOfCardWithTitle( $updateInfo["voiceCardName"], "VOICE" );
            $this->Log( "Card index: " . $voiceCardIndex );

            $this->sessionArray["players"][$playerKey]["voiceCardIndex"] = $voiceCardIndex;
        }
        
        //$this->RemoveCardsFromPlayers( array( "from" => "all", "type" => "VOICE" ) );

        $this->SaveGameState();
    }

    // $this->DrawCards( $_POST["draw-type"], $_POST["how-many-cards"] );
    function DrawCards( $type, $count ) {
        $this->sessionArray["drawnCards"] = array();

        for ( $i = 0; $i < $count; $i++ ) {
            $cardInfo = array();
            $cardInfo["index"] = $this->GetCardIndex( $type );
            $cardInfo["type"] = $type;
            array_push( $this->sessionArray["drawnCards"], $cardInfo );
        }

        // Doesn't return anything; it's stored in the session.

        $this->SaveGameState();
    }

    function ClearDrawnCards() {
        $this->sessionArray["drawnCards"] = array();
        $this->SaveGameState();
    }

    function ClearCardsUsed() {
        $this->sessionArray["cardsUsed"] = array();
        $this->SaveGameState();
    }

    function DealCards( $infoArray ) {
        $this->Log( "Deal " . $infoArray["quantity"] . " cards of type " . $infoArray["type"] . " to " . $infoArray["to"] );
        
        // Get indices to update
        $playerIndices = array();
        if ( $infoArray["to"] == "all" ) { 
            $playerIndices = $this->GetAllPlayerIndices();
        }
        else {
            array_push( $playerIndices, $this->GetIndexOfPlayerWithName( $infoArray["to"] ) );
        }        
        
        foreach( $playerIndices as $index ) {
            for ( $i = 0; $i < $infoArray["quantity"]; $i++ ) {
                $cardIndex = $this->GetCardIndex( $infoArray["type"] );
                $this->Log( "Give card " . $cardIndex . " to player " . $index );
                array_push( $this->sessionArray["players"][$index]["hand"][$infoArray["type"]], $cardIndex );
            }
        }

        $this->SaveGameState();
    }
    
    function RemoveCardsFromPlayers( $infoArray ) {
        // Get indices to update
        $playerIndices = array();
        if ( $infoArray["from"] == "all" ) { 
            $playerIndices = $this->GetAllPlayerIndices();
        }
        else {
            array_push( $playerIndices, $this->GetIndexOfPlayerWithName( $infoArray["from"] ) );
        }
        
        foreach( $playerIndices as $index ) {
            $this->sessionArray["players"][$index]["hand"][$infoArray["type"]] = array();
        }

        $this->SaveGameState();
    }

    function GetAllPlayerIndices() {
        $indices = array();
        foreach( $this->sessionArray["players"] as $index => $player ) {
            array_push( $indices, $index );
        }
        return $indices;
    }
    
    function FlagSafety() {
        $this->sessionArray["safety"] = true;
        $this->SaveGameState();
    }
    
    function RemoveSafety() {
        unset( $this->sessionArray["safety"] );
        $this->SaveGameState();
    }

    function UpdateBackdrop( $infoArray ) {
        $this->sessionArray["backdrop"] = $infoArray;
        $this->SaveGameState();
    }
    
    // $this->AddNewWord( $_POST["vocabulary"] );
    function AddNewWord( $infoArray ) {
        $this->Log( "Add new word \"" . $infoArray["word"] . "\", created by player " . $infoArray["playerName"] );
        
        if ( !isset( $this->sessionArray["gameboard"] ) ) {
            $this->sessionArray["gameboard"] = array();
            $this->sessionArray["gameboard"]["age1"] = array();
            $this->sessionArray["gameboard"]["age2"] = array();
            $this->sessionArray["gameboard"]["age3"] = array();
            $this->sessionArray["gameboard"]["legacy"] = array();
        }
        
        $this->Log( "Create new word: \"" . $infoArray["word"] . "\"" );
        // Create a new word entry
        $newWord = array();
        $newWord["word"] = $infoArray["word"];
        $newWord["meaning"] = $infoArray["meaning"];
        $newWord["relatedAspects"] = $infoArray["related"];
        
        // Add it to the appropriate age
        array_push( $this->sessionArray["gameboard"][$infoArray["age"]], $newWord );
        
        $this->SaveGameState();
    }
    
    // $this->RemoveCardFromPlayer( $_POST["removeCard"] );
    function RemoveCardFromPlayer( $infoArray ) {
        // One item in the array is marked with [checked] = "checked". Find that one
        $removeInfo = array();
        foreach ( $infoArray as $key => $data ) {
            if ( isset( $data["checked"] ) ) {
                $removeInfo = $data;
                break;
            }
        }
        
        $this->Log( "Remove card key " . $removeInfo["cardKey"] . " from player " . $removeInfo["playerName"] );
        $this->RemoveACardFromPlayer( $removeInfo["playerId"], $removeInfo["cardType"], $removeInfo["cardKey"] );
        
        $this->SaveGameState();
    }
    
    // $this->RemoveTermCard( $_POST["removeTerm"] );
    function RemoveTermCard( $infoArray ) {
        unset( $this->sessionArray["gameboard"][$infoArray["age"]][$infoArray["cardIndex"]] );        
        $this->SaveGameState();
    }
    
    // $this->UpdateWords( $_POST["term"] );
    function UpdateWords( $infoArray ) {
        foreach ( $infoArray as $ageName => $ageData ) {
            foreach( $ageData as $key => $updateInfo ) { 
                $this->sessionArray["gameboard"][$ageName][$key]["word"] = $updateInfo["word"];
                $this->sessionArray["gameboard"][$ageName][$key]["meaning"] = $updateInfo["meaning"];
            }
        }       
        $this->SaveGameState();
    }
    
    // $this->UpdateNotes( $_POST["game-notes"] );
    function UpdateNotes( $notes ) {
        $this->sessionArray["notes"] = $notes;
        $this->SaveGameState();
    }
    
    
    
    // HELPERS
    /*
        $this->cardsArray = array(
        "VOICE" => array(),
        "STORY-1" => array(),
        "STORY-2" => array(),
        "STORY-3" => array(),
        "LEGACY" => array()
        );
        *
        * $this->sessionArray["cardsUsed"]
     * */
    function GetVoiceCardIndex()  {       return $this->GetCardIndex( "VOICE" );   }
    function GetStory1CardIndex() {       return $this->GetCardIndex( "STORY-1" ); }
    function GetStory2CardIndex() {       return $this->GetCardIndex( "STORY-2" ); }
    function GetStory3CardIndex() {       return $this->GetCardIndex( "STORY-3" ); }
    function GetLegacyCardIndex() {       return $this->GetCardIndex( "LEGACY" );  }

    function GetCardIndex( $type ) {
        if ( sizeof( $this->cardsArray[$type] ) == sizeof( $this->sessionArray["cardsUsed"][$type] ) ) {
            // We are out of cards!!
            $this->Log( "No more cards of type \"" . $type . "\"!" );
            return "OUT-OF-CARDS";
        }

        // Find a card that hasn't been drawn before
        $random = rand( 0, sizeof( $this->cardsArray[$type] ) - 1 );

        // Hopefully won't cause infinite loop :P
        $rage = 0;
        while ( in_array( $random, $this->sessionArray["cardsUsed"][$type] ) ) {
            $random = rand( 1, sizeof( $this->cardsArray[$type] ) - 1 );
            $rage++;
            if ( $rage >= 100 ) { break; } // don't break the website, ok?
        }

        array_push( $this->sessionArray["cardsUsed"][$type], $random );

        return $random;
    }

    
    function RemoveACardFromPlayer( $playerId, $cardType, $cardId ) {
        $this->Log( "Remove card ID " . $cardId . " of type " . $cardType . " from player " . $playerId );
        
        $removeIndex = -1;
        foreach ( $this->sessionArray["players"][$playerId]["hand"][$cardType] as $index => $cardKey ) {
            $this->Log( "Card " . $index . " is Card Key " . $cardKey );
            
            if ( $cardKey == $cardId ) {
                $removeIndex = $index;
                break;
            }
        }
        
        $this->Log( "Card index is " . $removeIndex );
        
        if ( $removeIndex != -1 ) {
            unset( $this->sessionArray["players"][$playerId]["hand"][$cardType][$removeIndex] );
        }
        
        $this->SaveGameState();
    }
    
    function GetCardWithTypeAndIndex( $type, $index ) {
        return $this->cardsArray[$type][$index];
    }

    function GetIndexOfCardWithTitle( $title, $type ) {
        for ( $i = 0; $i < sizeof( $this->cardsArray[$type] ); $i++ ) {
            if ( strtoupper( $this->cardsArray[$type][$i]["TITLE"] ) == strtoupper( $title ) ) {
                return $i;
            }
        }
        return -1;
    }

    function GetIndexOfPlayerWithName( $name ) {
        for ( $i = 0; $i < sizeof( $this->sessionArray["players"] ); $i++ ) {
            if ( strtoupper( $this->sessionArray["players"][$i]["playerName"] ) == strtoupper( $name ) ) {
                return $i;
            }
        }
        return -1;
    }
    
    function SetupBackdropInfo() {
        $this->backdropInfo = array(
            "The Outpost" => array(
                "Description" => "The story of the first Martian outpost. After communications with Earth have been cut off, they fight to survive on the Red Planet.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-080.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-081.jpg",
                "category" => "Core"
            ),
            "The Compound" => array(
                "Description" => "A group has barricaded itself against a world they can no longer take part in. The year is 1982 and in their solitude, they will create a new Utopian home.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-082.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-083.jpg",
                "category" => "Core"
            ),
            "Sing the Earth Electric" => array(
                "Description" => "Left behind for one final mission before humanity abandoned Earth, a misfit crew of machines pursue their ultimate duty.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-084.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-085.jpg",
                "category" => "Core"
            ),
            "Thieves' Cant" => array (
                "Description" => "Scoundrels and thieves must develop a way to communicate with each other in public without betraying their real intentions.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-086.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-087.jpg",
                "category" => "Core"
            ),
            
            "Sancutary Island" => array (
                "Description" => "The sickness tore through Boston. In the shadow of the lighthouse on their island home, this group is determined to remain untouched by the disease.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-092.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-093.jpg",
                "category" => "Contributed"
            ),
            
            "The Czaten Dacha" => array (
                "Description" => " 	Always on the move, they make their homes in their carts and caravans. Distrusted by outsiders, locals refer to them as the Czaten Dacha, or horse people.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-094.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-095.jpg",
                "category" => "Contributed"
            ),
            
            "Worcester School" => array (
                "Description" => "They were sent to a remote English boarding school to become men. Instead, in their isolation, they found identity, community, and resistance.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-096.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-097.jpg",
                "category" => "Contributed"
            ),
            
            "Wolf Pack" => array (
                "Description" => "They protect their pack and their forest. Hunters in the night, the wolves use their bodies as well as their voices to communicate.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-098.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-099.jpg",
                "category" => "Contributed"
            ),
            
            "2081, Solar Slums" => array (
                "Description" => "In these neon streets, refusing the cultural suicide of corporate life means making your own way. Shaded from the deadly sun, this resistance of cyborgs and hackers will fight with every tool they can against a consumerist dictatorship.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-100.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-101.jpg",
                "category" => "Contributed"
            ),
            
            "The Protecting Ones" => array (
                "Description" => "The Swimming Ones, the Flying Ones, and the Crawling Ones are in danger. It is up to the Protecting Ones to safeguard the waters of the world.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-102.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-103.jpg",
                "category" => "Contributed"
            ),
            
            "Slave Uprising" => array (
                "Description" => "They claimed their freedom in blood. With their captors now buried on the island shores, how will they rule over their new home?",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-104.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-105.jpg",
                "category" => "Contributed"
            ),
            
            "Forbidden Children" => array (
                "Description" => "Unseen and discarded, these children must move in the sprawling hidden spaces of the city. May they have a moment to dream in the cracks and shadows.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-106.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-107.jpg",
                "category" => "Contributed"
            ),
            
            "Velayuthapuram, Tamil Nadu 2006" => array (
                "Description" => "Punished for no crime, this Dalit community in rural India is imprisoned in their own home. They must relish small victories and moments of happiness in order to survive in the face of cruel oppression.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-108.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-109.jpg",
                "category" => "Contributed"
            ),
            
            "Beyond the Village" => array (
                "Description" => "This band of queer artists, laborers, and hustlers make their homes in former parking garages of an urban hellscape that stretches forever. They must nurture and care for one another while fighting back against those who would prey on them.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-110.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-111.jpg",
                "category" => "Contributed"
            ),
            
            "The Self-Actualizataion Project" => array (
                "Description" => "They didn’t fit in with the rest of the world. Scattered across the globe, but connected digitally, they will form new lives with their families of choice.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-112.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-113.jpg",
                "category" => "Contributed"
            ),
            
            "Toybox Tales" => array (
                "Description" => "As the years go on, playtime grows shorter. What is to become of the toys when the Child they love so dearly grows up?",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-114.jpg",
                "Link2" => "http://www.moosader.com/web-games/dialect-tabletop/rulebook/Dialect-page-115.jpg",
                "category" => "Contributed"
            ),
            
            "The Quest" => array (
                "Description" => " 	A Final Fantasy inspired backdrop.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/scenarios/custom-the-quest.jpg",
                "Link2" => "",
                "category" => "Rachel's"
            ),
            
            "Survival Island" => array (
                "Description" => "A Reality TV show inspired backdrop.",
                "Link1" => "http://www.moosader.com/web-games/dialect-tabletop/scenarios/custom-survival-island.jpg",
                "Link2" => "",
                "category" => "Rachel's"
            ),
        );
    }

}

$game = new Game();
?>
